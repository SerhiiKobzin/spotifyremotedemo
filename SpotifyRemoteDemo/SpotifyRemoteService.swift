//
//  SpotifyRemoteService.swift
//  SpotifyRemoteDemo
//
//  Created by Serhii Kobzin on 14.05.2021.
//

import Foundation

enum PlayerState {
    case disconnected
    case paused(title: String)
    case playing(title: String)
}

class SpotifyRemoteService: NSObject {
    static let shared = SpotifyRemoteService()
    
    static let clientID = "76f795cf037c4bc69fd99785531b3cb7"
    static let redirectURL = URL(string: "spotify-ios-quick-start://spotify-login-callback")!
    
    private lazy var configuration = SPTConfiguration(clientID: SpotifyRemoteService.clientID, redirectURL: SpotifyRemoteService.redirectURL)
    private var playerStateChangesCallbackHandler: ((PlayerState) -> Void)?
    
    private var accessToken: String? {
        get {
            UserDefaults.standard.string(forKey: "accessToken")
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "accessToken")
        }
    }
    
    private var playerState: PlayerState = .disconnected {
        didSet {
            playerStateChangesCallbackHandler?(playerState)
        }
    }
    
    private lazy var appRemote: SPTAppRemote = {
        let appRemote = SPTAppRemote(configuration: configuration, logLevel: .debug)
        appRemote.connectionParameters.accessToken = self.accessToken
        appRemote.delegate = self
        return appRemote
    }()
    
    func handle(urlContexts: Set<UIOpenURLContext>) {
        guard let url = urlContexts.first?.url else { return }
        let parameters = appRemote.authorizationParameters(from: url)
        if let errorDescription = parameters?[SPTAppRemoteErrorDescriptionKey] {
            print(errorDescription)
            return
        }
        if let accessToken = parameters?[SPTAppRemoteAccessTokenKey] {
            appRemote.connectionParameters.accessToken = accessToken
            self.accessToken = accessToken
            connect()
        }
    }
    
    func connect() {
        if appRemote.isConnected { return }
        if appRemote.connectionParameters.accessToken == nil { return }
        appRemote.connect()
    }
    
    func disconnect() {
        guard appRemote.isConnected else { return }
        appRemote.disconnect()
    }
    
    func play(failureHandler: @escaping () -> Void) {
        switch playerState {
        case .disconnected:
            guard appRemote.isConnected else {
                if appRemote.authorizeAndPlayURI("") { return }
                failureHandler()
                return
            }
        case .paused:
            appRemote.playerAPI?.resume({ result, error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
            })
        case .playing:
            appRemote.playerAPI?.pause({ result, error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
            })
        }
    }
    
    func playNext() {
        appRemote.playerAPI?.skip(toNext: { result, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
        })
    }
    
    func playPrevious() {
        appRemote.playerAPI?.skip(toPrevious: { result, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
        })
    }
    
    func subscribeOnPlayerStateChanges(callbackHandler: @escaping (PlayerState) -> Void) {
        playerStateChangesCallbackHandler = callbackHandler
        playerStateChangesCallbackHandler?(playerState)
    }
}

// MARK: - SPTAppRemoteDelegate
extension SpotifyRemoteService: SPTAppRemoteDelegate {
    func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
        appRemote.playerAPI?.delegate = self
        appRemote.playerAPI?.subscribe(toPlayerState: { result, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
        })
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) { }
    
    func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
        playerState = .disconnected
    }
}

// MARK: - SPTAppRemotePlayerStateDelegate
extension SpotifyRemoteService: SPTAppRemotePlayerStateDelegate {
    func playerStateDidChange(_ playerState: SPTAppRemotePlayerState) {
        let title = "\(playerState.track.artist.name) - \(playerState.track.name)"
        self.playerState = playerState.isPaused ? .paused(title: title) : .playing(title: title)
    }
}
